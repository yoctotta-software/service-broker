import { Message } from "../message";
import { Transport, TransportOption } from "./transport";
import PubSub from "pubsub-js";

export class LocalTransport implements Transport {
  subscribeByAction(
    channel: string,
    action: string,
    onMessage: (msg: Message) => void
  ): Promise<string> {
    return new Promise((resolve, reject) =>
      resolve(
        PubSub.subscribe(channel, (msg: Message) => {
          if (msg.msgType === action) onMessage(msg);
        })
      )
    );
  }

  subscribe(
    channel: string,
    onMessage: (msg: Message) => void
  ): Promise<string> {
    return new Promise((resolve, reject) =>
      resolve(PubSub.subscribe(channel, onMessage))
    );
  }

  publish = (msg: Message): Promise<void> =>
    new Promise((resolve, reject) =>
      PubSub.publish(msg.channel, msg)
        ? resolve()
        : reject({ msg: "Couldn't publish" })
    );

  connect = (option?: TransportOption): Promise<void> =>
    new Promise((resolve, reject) => {
      resolve();
    });
}

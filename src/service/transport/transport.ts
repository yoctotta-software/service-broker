import { Message } from "../message";

export interface Transport {
  publish(msg: Message): Promise<void>;
  subscribe(
    channel: string,
    onMessage: (msg: Message) => void
  ): Promise<string>;
  subscribeByAction(
    channel: string,
    action: string,
    onMessage: (msg: Message) => void
  ): Promise<string>;
  connect(option?: TransportOption): Promise<void>;
}

export interface TransportOption {
  onConnect?: Function;
  onDisconnect?: Function;
  onReconnect?: Function;
  reconnect: boolean;
}

export interface Message {
  messageId: string;
  channel: string;
  data?: any;
  msgTime: Date;
  msgType: string;
}

import { Transport, TransportOption } from "../service/transport/transport";

export interface ServiceBroker {
  /**
   * The Bind method to be called by the Service in order to register it self with the broker
   * @param options: ServiceOption
   */
  bind(options: Service): Promise<void>;

  get(service: string): Promise<Service>;
}

/**
 * Service option to set up a service in a broker
 */
export interface Service {
  name: string;
  id: string;
  transport: Transport;
  transportOptions?: TransportOption;
  options?: {};

  busy: boolean;
  close(): Promise<void>;

  open(): Promise<Service>;

  isOpen(): Promise<boolean>;
}

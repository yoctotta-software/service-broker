import { uuid } from "uuidv4";
import { LocalTransport } from "../service/transport/local-transport";
import { Transport, TransportOption } from "../service/transport/transport";
import { ServiceBroker, Service } from "./service-broker";

export class LocalServiceBroker implements ServiceBroker {
  services: Map<string, Service[]> = new Map();

  get(service: string): Promise<Service> {
    return new Promise((resolve, reject) => {
      const serviceList = this.services.get(service);
      if (serviceList) {
        const availableService = serviceList.find(
          async (s) => !(await s.isOpen())
        );
        if (availableService) resolve(availableService.open());
        else reject({ msg: "Service not available" });
      } else reject({ msg: "Service not found" });
    });
  }

  bind(service: Service): Promise<void> {
    if (this.services.has(service.name)) {
      const serviceList = this.services.get(service.name);
      if (!serviceList) throw { msg: "Service not found" };
      serviceList.push(service);
    } else this.services.set(service.name, [service]);
    return service.transport.connect(service.transportOptions);
  }
}

export class LocalService implements Service {
  isOpen(): Promise<boolean> {
    return new Promise((resolve, reject) => {
      resolve(this.busy);
    });
  }
  open(): Promise<Service> {
    return new Promise((resolve, reject) => {
      this.busy = true;
      resolve(this);
    });
  }
  name!: string;
  id!: string;
  transport!: Transport;
  transportOptions?: TransportOption;
  options?: {};
  busy: boolean = false;

  constructor(name: string) {
    this.name = name;
    this.id = uuid();
    this.transport = new LocalTransport();
  }

  close(): Promise<void> {
    return new Promise((resolve, reject) => {
      this.busy = false;
      resolve();
    });
  }
}

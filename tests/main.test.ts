import { expect } from "chai";
import { LocalService, LocalServiceBroker } from "../src/broker/local-broker";
import { Message } from "../src/service/message";

const broker = new LocalServiceBroker();

describe("Test Subscribe", function () {
  it("Subscribe All", function (done) {
    new Promise<Message>((resolve, reject) => {
      broker
        .bind(new LocalService("test"))
        .then(() => broker.get("test"))
        .then((service) => {
          service.transport.subscribe("testChannel", (msg) => {
            console.log(msg);
            resolve(msg);
          });
          service.transport
            .publish({
              messageId: "test1",
              channel: "testChannel",
              data: { msg: "Some Data" },
              msgTime: new Date(),
              msgType: "",
            })
            .then(
              () => console.log("Published"),
              (reason) => console.log(reason)
            );
        })
        .then(done);
    });
  });
  it("Subscribe Action", function (done) {
    new Promise<Message>((resolve, reject) => {
      broker
        .bind(new LocalService("test"))
        .then(() => broker.get("test"))
        .then((service) => {
          service.transport.subscribeByAction(
            "testChannel",
            "action1",
            (msg) => {
              console.log(msg);
              if (msg.msgType === "action1") resolve(msg);
              else reject({ msg: "Error" });
            }
          );
          service.transport
            .publish({
              messageId: "test1",
              channel: "testChannel",
              data: { msg: "Some Data" },
              msgTime: new Date(),
              msgType: "action1",
            })
            .then(
              () => console.log("Published"),
              (reason) => console.log(reason)
            );
          service.transport
            .publish({
              messageId: "test1",
              channel: "testChannel",
              data: { msg: "Some Data" },
              msgTime: new Date(),
              msgType: "action2",
            })
            .then(
              () => console.log("Published"),
              (reason) => console.log(reason)
            );
          service.transport
            .publish({
              messageId: "test1",
              channel: "testChannel",
              data: { msg: "Some Data" },
              msgTime: new Date(),
              msgType: "action1",
            })
            .then(
              () => console.log("Published"),
              (reason) => console.log(reason)
            );
        })
        .then(done);
    });
  });
});

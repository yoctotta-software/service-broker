yoctotta-service-broker

Its a service broker that allows for registering services and using a Pub/Sub mechanism in node js.
E.G.

```
const broker = new LocalServiceBroker();

broker
        .bind(new LocalService("test"))
        .then(() => broker.get("test"))
        .then((service) => {
          service.transport.subscribe("testChannel", (msg) => {
            console.log(msg);
            resolve(msg);
          });
          service.transport
            .publish({
              messageId: "test1",
              channel: "testChannel",
              data: { msg: "Some Data" },
              msgTime: new Date(),
              msgType: "",
            })
            .then(
              () => console.log("Published"),
              (reason) => console.log(reason)
            );
        })
```
